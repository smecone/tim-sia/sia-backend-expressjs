module.exports = (grunt) => {
    /**
     * Initialize Grunt Configurations
     */
    grunt.initConfig({
        watch: {
            default: {
                files: ['*.ts', '*.js', '*.json', '*.env'],
                tasks: ['default']
            },
            ts: {
                files: [
                    'app/**/*.ts', 'app/controllers/**/*.ts', 'app/models/**/*.ts', 'app/types/**/*.ts',
                    'database/**/*.ts', 'sys/**/*.ts'
                ],
                tasks: ['default']
            }        
        },
        copy: {
            env: {
                expand: true,
                cwd: '.',
                src: '.env',
                dest: 'dist/'
            }
        },
        ts: {
            default: {
                src: [
                    
                ],
                tsconfig: {
                    tsconfig: './tsconfig.json',
                    passThrough: true
                }
            },
            options: {
                sourceMap: false
            }
        },
        tslint: {
            options: {
                configuration: 'tslint.json',
                force: true,
                fix: true
            },
            files: [
                'app/**/*.ts', 'app/controllers/**/*.ts', 'app/models/**/*.ts', 'app/types/**/*.ts',
                'database/**/*.ts', 'sys/**/*.ts'
            ],
        }
    })

    /**
     * Grunt Events
     */
    grunt.event.on('watch', () => {
        
    });

    /**
     * Grunt Load Task Modules
     */
    grunt.loadNpmTasks('grunt-ts');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-tslint');

    /**
     * Grunt Tasks Register
     */    
    grunt.registerTask('default', ['tslint', 'ts', 'copy']);
}
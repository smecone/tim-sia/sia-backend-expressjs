import { Request, Response } from "express";
import { IController } from "../../sys/controller";
import { Petugas as MPetugas } from "../models/Petugas.model";

export class Petugas implements IController {
    public index(request: Request, response: Response): void {
        MPetugas.findAll()
            .then((petugass) => {
                response.json(petugass);
            })
            .catch((error) => {
                response.json(error);
            });
    }
    public create(request: Request, response: Response): void {
        throw new Error("Method not implemented.");
    }
    public store(request: Request, response: Response): void {
        MPetugas.create(request.body)
            .then((success) => {
                response.json(success);
            })
            .catch((error) => {
               response.json(error);
            });
    }
    public view(request: Request, response: Response): void {
        throw new Error("Method not implemented.");
    }
    public edit(request: Request, response: Response): void {
        throw new Error("Method not implemented.");
    }
    public update(request: Request, response: Response): void {
        MPetugas.update(request.body, {
            where: {
                id: request.body.id,
            },
        })
            .then((success) => {
                response.json(success);
            })
            .catch((error) => {
                response.json(error);
            });
    }
    public destroy(request: Request, response: Response): void {
        MPetugas.destroy({
            where: {
                id: request.body.id,
            },
        })
            .then((success) => {
                response.json(success);
            })
            .catch((error) => {
                response.json(error);
            });
    }

}

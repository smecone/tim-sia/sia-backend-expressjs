// import { database as sequelize } from "../../sys/app";
// import { Sequelize, DataTypes } from "sequelize";
// const Sequelize: DataTypes = require("sequelize");
import { Model, Sequelize } from "sequelize-typescript";
import { AutoIncrement } from "sequelize-typescript/lib/annotations/AutoIncrement";
import { Column } from "sequelize-typescript/lib/annotations/Column";
import { PrimaryKey } from "sequelize-typescript/lib/annotations/PrimaryKey";
import { Table } from "sequelize-typescript/lib/annotations/Table";
import { DataType } from "sequelize-typescript/lib/enums/DataType";

@Table({
    tableName: "petugas",
    timestamps: false,
    underscored: true,
})
export class Petugas extends Model<Petugas> {
    @PrimaryKey
    @AutoIncrement
    @Column(DataType.INTEGER)
    public id: number;

    @Column(DataType.STRING)
    public nama: string;

    @Column(DataType.STRING)
    public username: string;

    @Column(DataType.STRING)
    public email: string;

    @Column(DataType.STRING)
    public password: string;

}

// module.exports = sequelize.define("petugas", {
//     id: {
//         type: Sequelize.INTEGER,
//         primaryKey: true,
//         autoIncrement: true
//     },
//     nama: Sequelize.STRING,
//     username: Sequelize.STRING,
//     email: Sequelize.STRING,
//     password: Sequelize.STRING
// }, {
//     timestamps: false,
//     underscored: true
// })

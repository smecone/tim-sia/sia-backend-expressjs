import { Express, Request, Response, Router } from "express";
import { request } from "http";
import { Informasi } from "./controllers/informasi";
import { Petugas } from "./controllers/petugas";
const express = require("express");

/*
    /index
    /informasi/{id}
    /jadwal/{kelas}/{tanggal}
    /petugas
    /
*/

/**
 * Petugas Routes
 */
const petugas: Router = express.Router();
const cpetugas = new Petugas();
petugas.get("/", cpetugas.index);
petugas.post("/store", cpetugas.store);
petugas.put("/update", cpetugas.update);
petugas.delete("/destroy", cpetugas.destroy);

/**
 * Informasi Routes
 */
const informasi: Router = express.Router();
const cinformasi = new Informasi();
informasi.get("/", cinformasi.index);

/**
 * Base Routes
 *
 * @param app
 */
const routes = (app: Express) => {
    /**
     * Middleware SetHeader
     */
    app.use((request: Request, response: Response, next) => {
        response.setHeader("Content-Type", "application/json");
        next();
    });

    app.get("/", (request: Request, response: Response) => {
        response.send("Hello It's Work :D");
        response.end();
    });

    app.use("/api/informasi", informasi);
    app.use("/api/petugas", petugas);
};

export default routes;

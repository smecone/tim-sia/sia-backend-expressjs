import * as path from "path";
import * as dotenv from "dotenv";
import { readFileSync } from "fs";

// Load environment variables .env file
const envPath = ".env";
const env = dotenv.config({path: envPath});
const envParsed = dotenv.parse(readFileSync(envPath));

const Sequelize = {
    "development": {
        "username": process.env.DB_USERNAME,
        "password": process.env.DB_PASSWORD,
        "database": process.env.DB_NAME,
        "host": process.env.DB_HOST,
        "dialect": process.env.DB_DIALECT,
        "omitNull": true 
    }
}

const Configs = {    
    env: envParsed,
    sequelize: Sequelize,
    development: Sequelize.development
}

export { Configs, Sequelize };
export default Configs;
module.exports = Configs;
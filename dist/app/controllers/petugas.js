"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Petugas_model_1 = require("../models/Petugas.model");
class Petugas {
    index(request, response) {
        Petugas_model_1.Petugas.findAll()
            .then((petugass) => {
            response.json(petugass);
        })
            .catch((error) => {
            response.json(error);
        });
    }
    create(request, response) {
        throw new Error("Method not implemented.");
    }
    store(request, response) {
        Petugas_model_1.Petugas.create(request.body)
            .then((success) => {
            response.json(success);
        })
            .catch((error) => {
            response.json(error);
        });
    }
    view(request, response) {
        throw new Error("Method not implemented.");
    }
    edit(request, response) {
        throw new Error("Method not implemented.");
    }
    update(request, response) {
        Petugas_model_1.Petugas.update(request.body, {
            where: {
                id: request.body.id,
            },
        })
            .then((success) => {
            response.json(success);
        })
            .catch((error) => {
            response.json(error);
        });
    }
    destroy(request, response) {
        Petugas_model_1.Petugas.destroy({
            where: {
                id: request.body.id,
            },
        })
            .then((success) => {
            response.json(success);
        })
            .catch((error) => {
            response.json(error);
        });
    }
}
exports.Petugas = Petugas;

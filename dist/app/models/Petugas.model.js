"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
// import { database as sequelize } from "../../sys/app";
// import { Sequelize, DataTypes } from "sequelize";
// const Sequelize: DataTypes = require("sequelize");
const sequelize_typescript_1 = require("sequelize-typescript");
const AutoIncrement_1 = require("sequelize-typescript/lib/annotations/AutoIncrement");
const Column_1 = require("sequelize-typescript/lib/annotations/Column");
const PrimaryKey_1 = require("sequelize-typescript/lib/annotations/PrimaryKey");
const Table_1 = require("sequelize-typescript/lib/annotations/Table");
const DataType_1 = require("sequelize-typescript/lib/enums/DataType");
let Petugas = class Petugas extends sequelize_typescript_1.Model {
};
__decorate([
    PrimaryKey_1.PrimaryKey,
    AutoIncrement_1.AutoIncrement,
    Column_1.Column(DataType_1.DataType.INTEGER),
    __metadata("design:type", Number)
], Petugas.prototype, "id", void 0);
__decorate([
    Column_1.Column(DataType_1.DataType.STRING),
    __metadata("design:type", String)
], Petugas.prototype, "nama", void 0);
__decorate([
    Column_1.Column(DataType_1.DataType.STRING),
    __metadata("design:type", String)
], Petugas.prototype, "username", void 0);
__decorate([
    Column_1.Column(DataType_1.DataType.STRING),
    __metadata("design:type", String)
], Petugas.prototype, "email", void 0);
__decorate([
    Column_1.Column(DataType_1.DataType.STRING),
    __metadata("design:type", String)
], Petugas.prototype, "password", void 0);
Petugas = __decorate([
    Table_1.Table({
        tableName: "petugas",
        timestamps: false,
        underscored: true,
    })
], Petugas);
exports.Petugas = Petugas;
// module.exports = sequelize.define("petugas", {
//     id: {
//         type: Sequelize.INTEGER,
//         primaryKey: true,
//         autoIncrement: true
//     },
//     nama: Sequelize.STRING,
//     username: Sequelize.STRING,
//     email: Sequelize.STRING,
//     password: Sequelize.STRING
// }, {
//     timestamps: false,
//     underscored: true
// })

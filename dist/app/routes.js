"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const informasi_1 = require("./controllers/informasi");
const petugas_1 = require("./controllers/petugas");
const express = require("express");
/*
    /index
    /informasi/{id}
    /jadwal/{kelas}/{tanggal}
    /petugas
    /
*/
/**
 * Petugas Routes
 */
const petugas = express.Router();
const cpetugas = new petugas_1.Petugas();
petugas.get("/", cpetugas.index);
petugas.post("/store", cpetugas.store);
petugas.put("/update", cpetugas.update);
petugas.delete("/destroy", cpetugas.destroy);
/**
 * Informasi Routes
 */
const informasi = express.Router();
const cinformasi = new informasi_1.Informasi();
informasi.get("/", cinformasi.index);
/**
 * Base Routes
 *
 * @param app
 */
const routes = (app) => {
    /**
     * Middleware SetHeader
     */
    app.use((request, response, next) => {
        response.setHeader("Content-Type", "application/json");
        next();
    });
    app.get("/", (request, response) => {
        response.send("Hello It's Work :D");
        response.end();
    });
    app.use("/api/informasi", informasi);
    app.use("/api/petugas", petugas);
};
exports.default = routes;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv = require("dotenv");
const fs_1 = require("fs");
// Load environment variables .env file
const envPath = ".env";
const env = dotenv.config({ path: envPath });
const envParsed = dotenv.parse(fs_1.readFileSync(envPath));
const Sequelize = {
    "development": {
        "username": process.env.DB_USERNAME,
        "password": process.env.DB_PASSWORD,
        "database": process.env.DB_NAME,
        "host": process.env.DB_HOST,
        "dialect": process.env.DB_DIALECT,
        "omitNull": true
    }
};
exports.Sequelize = Sequelize;
const Configs = {
    env: envParsed,
    sequelize: Sequelize,
    development: Sequelize.development
};
exports.Configs = Configs;
exports.default = Configs;
module.exports = Configs;

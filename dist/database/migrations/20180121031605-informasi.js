"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
module.exports = {
    up: (queryInterface, Sequelize) => {
        /*
          Add altering commands here.
          Return a promise to correctly handle asynchronicity.
    
          Example:
          return queryInterface.createTable('users', { id: Sequelize.INTEGER });
        */
        return queryInterface.createTable("informasi", {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            petugas_id: {
                type: Sequelize.INTEGER,
                references: {
                    model: "petugas",
                    key: "id",
                },
            },
            title: Sequelize.STRING,
            content: Sequelize.TEXT,
            // Timestamps
            created_at: Sequelize.DATE,
            updated_at: Sequelize.DATE,
        });
    },
    down: (queryInterface, Sequelize) => {
        /*
          Add reverting commands here.
          Return a promise to correctly handle asynchronicity.
    
          Example:
          return queryInterface.dropTable('users');
        */
        return queryInterface.dropTable("informasi");
    },
};

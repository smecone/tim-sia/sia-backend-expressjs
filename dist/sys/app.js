"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path = require("path");
// Express Requires
const bodyParser = require("body-parser");
const compression = require("compression");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const express = require("express");
const logger = require("morgan");
// Load Configurations
const Configs = require("../config");
// Database
const sequelize_typescript_1 = require("sequelize-typescript");
// Application
const routes = require("../app/routes");
// Initialize Database
//export { sequelize as database } from "./database";
// export var database = initializeDatabase.sequelize;
// initializeDatabase.modelsPath());
exports.database = new sequelize_typescript_1.Sequelize({
    host: Configs.env.DB_HOST,
    database: Configs.env.DB_NAME,
    dialect: Configs.env.DB_DIALECT,
    username: Configs.env.DB_USERNAME,
    password: Configs.env.DB_PASSWORD,
    storage: ":memory:",
    modelPaths: [path.resolve(__dirname, "../app/models/**/*.model.+(ts|js)")],
});
// Express Instance
const app = express();
exports.app = app;
// Express Configurations
app.set("port", process.env.PORT || 3000);
app.set("views", path.join(__dirname, "../views"));
app.set("view engine", "pug");
// Express
app.use(compression());
app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());
// Routes
routes.default(app);
// Testing Only
/*import Seq = require('Seq');
import { Seq } from "Seq";

var conn: Seq
var model: Seq.Model<{}, {}>

connections()
.then(() => {
    return models(conn)
})
.then(() => {
    console.log("Success");
})
.catch(err => {
    console.log(err)
})

function connections(): Promise<void>{
    return new Promise((resolve, rejected) => {
        conn = new Seq({
            database: Configs.env.DB_NAME,
            username: Configs.env.DB_USERNAME,
            password: Configs.env.DB_PASSWORD,
            dialect: 'mysql',
            host: 'homestead.vag',
            port: 3306,
            pool: {
                acquire: 30000,
                idle: 3000,
                max: 5,
                min: 0
            }
        })
    })
}

function models(conn: Seq): Promise<void>{
    return new Promise((resolve, rejected) => {
        model = conn.define('testing', {
            Column1: {
                type: Seq.STRING
            },
            Column2: {
                type: Seq.STRING
            }
        })
    })
}*/

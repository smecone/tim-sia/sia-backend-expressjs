"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const glob = require("glob");
const path = require("path");
const Seq = require("sequelize");
const Configs = require("../config");
exports.sequelize = new Seq(Configs.env.DB_NAME, Configs.env.DB_USERNAME, Configs.env.DB_PASSWORD, { dialect: Configs.env.DB_DIALECT });
glob(path.resolve(__dirname, "../app/models/**/*.model.+(ts|js)"), (err, files) => {
    files.forEach((file) => {
        exports.sequelize.import(file);
    });
});
//var initializeDatabase = new Database(instanceSeq);
// /*class DBSequelize {
//     private _sequelizeInstance: Sequelize;
//     constructor(sequelize: Sequelize) {
//         this._sequelizeInstance = sequelize;
//     }
//     get sequelize(): Sequelize {
//         return this._sequelizeInstance;
//     }
//     /**
//      * Add Models Path
//      *
//      * @param {*} pathModels
//      * @memberof DBSequelize
//      */
//     modelsPath(modelPaths: any) {
//         // const modelsPath = pathModels;
//         // fs.readdirSync(modelsPath).forEach(modelFile => {
//         //     const model = this.sequelize.import(path.join(modelsPath, modelFile));
//         // })
//     }
// }
//export { DBSequelize as Database }

"use strict";
const errorHandler = require("errorhandler");
const app_1 = require("./app");
/**
 * Error Handler
 */
app_1.app.use(errorHandler());
/**
 * Assign app instance
 */
const server = app_1.app;
/**
 * Start Express Server after Sync Sequelize
 */
//sequelize.sync().then(function() {
server.listen(app_1.app.get("port"), () => {
    console.log(("  App is running at %s:%d in %s mode"), "http://localhost", app_1.app.get("port"), app_1.app.get("env"));
    console.log("  Press CTRL-C to stop\n");
});
module.exports = server;

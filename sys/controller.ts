import { Request, Response } from "express";

export interface IController {
    index(request: Request, response: Response): void;
    create(request: Request, response: Response): void;
    store(request: Request, response: Response): void;
    view(request: Request, response: Response): void;
    edit(request: Request, response: Response): void;
    update(request: Request, response: Response): void;
    destroy(request: Request, response: Response): void;
}

export class Controller {
}

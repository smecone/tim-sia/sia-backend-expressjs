import * as fs from "fs";
import * as glob from "glob";
import * as path from "path";
import { Sequelize } from "sequelize";
const Seq = require("sequelize");
const Configs = require("../config");

export let sequelize: Sequelize = new Seq(
    Configs.env.DB_NAME,
    Configs.env.DB_USERNAME,
    Configs.env.DB_PASSWORD,
    { dialect: Configs.env.DB_DIALECT },
);

glob(path.resolve(__dirname, "../app/models/**/*.model.+(ts|js)"), (err, files) => {
    files.forEach((file) => {
        sequelize.import(file);
    });
});

//var initializeDatabase = new Database(instanceSeq);

// /*class DBSequelize {
//     private _sequelizeInstance: Sequelize;

//     constructor(sequelize: Sequelize) {
//         this._sequelizeInstance = sequelize;
//     }

//     get sequelize(): Sequelize {
//         return this._sequelizeInstance;
//     }

//     /**
//      * Add Models Path
//      *
//      * @param {*} pathModels
//      * @memberof DBSequelize
//      */
//     modelsPath(modelPaths: any) {
//         // const modelsPath = pathModels;
//         // fs.readdirSync(modelsPath).forEach(modelFile => {
//         //     const model = this.sequelize.import(path.join(modelsPath, modelFile));
//         // })

//     }
// }

//export { DBSequelize as Database }

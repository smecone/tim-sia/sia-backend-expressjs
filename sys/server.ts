import * as errorHandler from "errorhandler";

import { app, database as sequelize } from "./app";

/**
 * Error Handler
 */
app.use(errorHandler());

/**
 * Assign app instance
 */
const server = app;

/**
 * Start Express Server after Sync Sequelize
 */
//sequelize.sync().then(function() {
server.listen(app.get("port"), () => {
        console.log(
            ("  App is running at %s:%d in %s mode"),
            "http://localhost",
            app.get("port"),
            app.get("env"),
        );
        console.log("  Press CTRL-C to stop\n");
    });
//});

export = server;
